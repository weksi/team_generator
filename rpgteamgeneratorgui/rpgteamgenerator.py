'''
    Author: Ville-Veikko Vähäaho
    Random team generator for RPG group.
    With this program you can divide your Rpggroup, 
    or any team really, to two groups. In this version
    Gm's also are listed as players in eachothers group.

'''
 
# Import tkinter
from ast import Delete
from cgitb import text
from tkinter import *
import random
 
# Create the root window
root = Tk()
root.title("Ryhmäjako")
root.geometry('775x400')
frameright = Frame(root, width=300, height=300)
frameleft = Frame(root, width=300, height=300)
frameleft.grid(row=0, column=0)
frameright.grid(row=0, column=1)
 
# Create a listboxes for player and gm selection
listboxplayers = Listbox(frameleft, selectmode=MULTIPLE, exportselection=FALSE)
listboxgms = Listbox(frameleft, selectmode=MULTIPLE, exportselection=FALSE)

# create textbox
textbox = Text(frameright)
# Inserting the listbox items
file = open("pelaajat.txt", "r")
for rivi in file:
    rivinpoisto = rivi.rstrip("\r\n")
    listboxplayers.insert(END,rivinpoisto)
file = open("gmt.txt", "r")
for rivi in file:
    rivinpoisto = rivi.rstrip("\r\n")
    listboxgms.insert(END,rivinpoisto)

# Function for taking the selected listbox values and randomising them in to the teams and then inserting them to the textbox.
def selected():
    pelaajat = []
    gmt = []
    gm1pelaajat=[]
    gm2pelaajat=[]
    gm1 = []
    gm2 = []
    textbox.delete(1.0, END)
    # Add the selected listbox items to the list for use
    for i in listboxplayers.curselection():
       pelaajat.append(listboxplayers.get(i))
    for i in listboxgms.curselection(): 
       gmt.append(listboxgms.get(i))
    # Take the appended lists and use them for randomising gm:s and teams.
    # Randomising the gm:s
    while len(gmt)>0:
        a = random.choice(gmt)
        gm1.append(a)
        gmt.remove(a)
        if len(gmt)>0:	
            b = random.choice(gmt)
            gmt.remove(b)
            gm2.append(b)
    # Randomising the players
    while len(pelaajat)>0:
        a = random.choice(pelaajat)
        gm1pelaajat.append(a)
        pelaajat.remove(a)
        if len(pelaajat)>0:	
            b = random.choice(pelaajat)
            gm2pelaajat.append(b)
            pelaajat.remove(b)
    # inserting results to the textbox
    textbox.insert(END, f"GM:n {' '.join(gm1)} peliin tulevat: {', '.join(gm1pelaajat)}, {''.join(gm2)}\n")
    textbox.insert(END, f"GM:n {' '.join(gm2)} peliin tulevat: {', '.join(gm2pelaajat)}, {''.join(gm1)}")
    
       
 
# Create a button widget and command it to use selected function.
btn = Button(frameleft, text='Arvo', command=selected)
# Placing the button, listbox and the textbox
listboxplayers.pack()
listboxgms.pack()
btn.pack()
textbox.pack()
root.mainloop()
